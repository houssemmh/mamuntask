package mamun.backend.developerevalutation.services;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class FilesStorageServiceImpl implements FilesStorageService{

    private final Path root = Paths.get("uploads");
    @Override
    public void init() {
        try {
            Files.createDirectory(root);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }
    @Override
    public void save(MultipartFile file, String fileName) {
        try {
            Files.deleteIfExists(this.root.resolve(fileName));
            Files.copy(file.getInputStream(), this.root.resolve(fileName));
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }
    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public List<String> extractFirstColumn(String fileName, int n) throws IOException {
        Path path = this.root.resolve(fileName).toAbsolutePath();
        BufferedReader reader = new BufferedReader(new FileReader(path.toString()));

        List<String> firstColList= new ArrayList<>();
        String line;
        int i = 1;
        while ((line = reader.readLine()) != null && i<=n) {
            String[] cols = line.split(",");
            firstColList.add(cols[0].toString());
            i++;
        }
        reader.close();
        return firstColList;
    }

    @Override
    public List<String> addEmptyValues(List<String> list, int n) {
        int i = list.size();
        while(i<n){
            list.add("");
            i++;
        }
        return list;
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(root.toFile());
    }

    @Override
    public void createCSVFile(List<String> list1, List<String> list2,String fileName) throws IOException {
        FileWriter out = new FileWriter(this.root.resolve(fileName).toString());
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)) {
            int i ;
            for (i=0;i<=list1.size()-1;i++) {
                try {
                    printer.printRecord(list2.get(i), list1.get(i));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load the files!");
        }
    }

}
