package mamun.backend.developerevalutation.services;

import org.springframework.web.multipart.MultipartFile;

import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public interface FilesStorageService {

    public void init();
    public void save(MultipartFile file,String filename);
    public Resource load(String filename);
    public List<String> extractFirstColumn(String fileName, int n) throws IOException;
    public List<String> addEmptyValues(List<String> list, int n);
    public void deleteAll();
    public Stream<Path> loadAll();
    public void createCSVFile(List<String> list1,List<String> list2,String fileName) throws IOException;

}
