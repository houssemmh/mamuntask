package mamun.backend.developerevalutation.model;

public class FileInfo {

    private String name;
    private String url;

    private byte[] data;

    public FileInfo() {
    }

    public FileInfo(String name, byte[] data) {
        this.name = name;
        this.url = url;
    }

    public FileInfo(String filename, String url) {
        this.name = filename;
        this.url = url;
    }

    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
