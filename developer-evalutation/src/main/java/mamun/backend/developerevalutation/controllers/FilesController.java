package mamun.backend.developerevalutation.controllers;

import mamun.backend.developerevalutation.message.ResponseMessage;
import mamun.backend.developerevalutation.model.FileInfo;
import mamun.backend.developerevalutation.services.FilesStorageService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class FilesController {

    final String FILE1 = "file1.csv";
    final String FILE2 = "file2.csv";
    final String COMBINED_RESULT_FILE = "combined.csv";
    final String FULL = "FULL";
    final String NORMAL = "NORMAL";

    @Autowired
    FilesStorageService storageService;

    @PostMapping("process-data/")
    public ResponseEntity<?> uploadFile(@RequestParam("file1") MultipartFile file1,
                                                      @RequestParam("file2") MultipartFile file2,
                                                      @RequestParam("collationPolicy") String collationPolicy,
                                                      @RequestParam("n") int n) {

        try {
            //save files to uploads folder
            storageService.save(file1,FILE1);
            storageService.save(file2,FILE2);

            //Extract first column of first file
            List<String> list1 = new ArrayList<>();
            list1 = storageService.extractFirstColumn(FILE1,n);

            //Extract first column of second file
            List<String> list2 = new ArrayList<>();
            list2 = storageService.extractFirstColumn(FILE2,n);

            if(this.FULL.equals(collationPolicy)){
                if(list1.size() < n){
                    list1 = storageService.addEmptyValues(list1,n);
                }
                if(list2.size() < n){
                    list2 = storageService.addEmptyValues(list2,n);
                }
            }else{
                if(list1.size() > list2.size()){
                    list1 = list1.subList(0,list2.size());
                }else{
                    list2 = list2.subList(0,list1.size());
                }
            }

            //Reverse the second list
            Collections.reverse(list2);

            storageService.createCSVFile(list1,list2,COMBINED_RESULT_FILE);
            System.out.println("FINISHED");

            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("DATA UPLOAD SUCCESSFULLY"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(e.getMessage()));
        }
    }

    @GetMapping("/files/{filename}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = storageService.load(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/files")
    public ResponseEntity<List<FileInfo>> getListFiles() {
        List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
            String filename = path.getFileName().toString();
            String url = MvcUriComponentsBuilder
                    .fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();
            return new FileInfo(filename, url);
        }).collect(Collectors.toList());
        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
    }

    private final Path root = Paths.get("uploads");
    @GetMapping("/get-file/{fileName}")
    public ResponseEntity<?> downloadFile(@PathVariable String fileName, HttpServletResponse res) throws IOException {

        FileInfo fileInfo = new FileInfo();
        fileInfo.setName(fileName);
        fileInfo.setData(contentOf(fileName));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(fileInfo);
    }

    private byte[] contentOf(String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(storageService.load(fileName).getURI()));
    }
}
