import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { DataProcessingService } from 'src/app/services/data-processing.service';
import { CollationOptions } from 'src/app/utils/collation-options';


@Component({
  selector: 'app-data-processing',
  templateUrl: './data-processing.component.html',
  styleUrls: ['./data-processing.component.scss']
})
export class DataProcessingComponent implements OnInit {

  public collationOptions: CollationOptions[] = [];
  public selectedCollationOption: CollationOptions;
  public download: boolean = false;
  public submitted:boolean = false;
  public numberOfLines: number = 5;
  public file1: File;
  public file2: File;
  progress = 0;
  message = '';
  readonly COMBINED_RESULT_FILE: string = "combined.csv";
  public resultFile: any;

  constructor(private dataProcessingService: DataProcessingService, private messageService:MessageService) { }

  ngOnInit(): void {
    this.collationOptions = [{ label: "Full", option: "full" }, { label: "Normal", option: "normal" }];
    console.log(this.collationOptions);
  }

  uploadFirstFile(event: any) {
    this.file1 = event.files[0];
  }

  uploadSecondFile(event: any) {
    this.file2 = event.files[0];
  }

  public processData() {
    this.submitted= true;
    this.resultFile = "";
    if(this.file1 && this.file2 && this.selectedCollationOption && this.numberOfLines){
      this.dataProcessingService.processData(this.file1, this.file2, this.selectedCollationOption.option, this.numberOfLines).subscribe(
        data => {
          if (this.download) {
            window.open(this.dataProcessingService.downloadFile(this.COMBINED_RESULT_FILE));
          }else{
            this.dataProcessingService.readFile(this.COMBINED_RESULT_FILE).subscribe(data=>{
              this.resultFile = atob(data.data);
              this.messageService.add({severity:"success",summary:"Finished processing data!"})
            },err=>{
              this.messageService.add({severity:"error",summary:"Something went wrong ! please retry later"})
              console.log(err)
            })
          }
        },
        error => {
          console.error("ERROR PROCESSING DATA : ", error);
        }
      )
    }
    
  }

}
