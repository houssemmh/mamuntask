import { HttpClient, HttpEvent, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class DataProcessingService {

  readonly serverUrl = environment.server;

  constructor(private http: HttpClient) { }

  public processData(file1:File,file2:File,collationPolicy:string,numberOfLines):Observable<HttpEvent<any>>{
    let formData = new FormData();
    formData.append('file1',file1);
    formData.append('file2',file2);
    formData.append('collationPolicy',collationPolicy);
    formData.append('n',numberOfLines);

    return this.http.post<any>(this.serverUrl+ 'process-data/',formData,{
      reportProgress: true,
      responseType: 'json'
    });
  }

  public downloadFile(fileName:string): any {
    return this.serverUrl+'files/'+fileName;
  }

  public readFile(fileName:string): any {
    return this.http.get<any>(this.serverUrl+'get-file/'+fileName,);
  }
  
}
