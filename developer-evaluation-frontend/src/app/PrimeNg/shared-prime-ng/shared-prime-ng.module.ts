import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FileUploadModule} from 'primeng/fileupload';
import {DropdownModule} from 'primeng/dropdown';
import {CheckboxModule} from 'primeng/checkbox';
import { MessageService } from 'primeng/api';
import {ToastModule} from 'primeng/toast';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FileUploadModule,
    DropdownModule,
    CheckboxModule,
    ToastModule
  ],
  exports:[
    FileUploadModule,
    DropdownModule,
    CheckboxModule,
    ToastModule
  ],
  providers:[
    MessageService
  ]
})
export class SharedPrimeNGModule { }
