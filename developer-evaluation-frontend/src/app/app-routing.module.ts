import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataProcessingComponent } from './components/data-processing/data-processing.component';

const routes: Routes = [
  { path: 'data-processing', component: DataProcessingComponent },
  { path: '', redirectTo: '/data-processing', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
