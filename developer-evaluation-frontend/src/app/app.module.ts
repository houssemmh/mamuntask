import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { SharedComponentsModule } from './shared/shared-components.module';
import { SharedPrimeNGModule } from './PrimeNg/shared-prime-ng/shared-prime-ng.module';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataProcessingComponent } from './components/data-processing/data-processing.component';
import { MessageService } from 'primeng/api';

@NgModule({
  declarations: [
    AppComponent,
    DataProcessingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedComponentsModule,
    SharedPrimeNGModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
