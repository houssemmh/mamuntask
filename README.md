**Prerequisite** :

- Java 11
- Maven
- Docker
- Docker-compose

**Assumption** : 
- CSV files must have comma separeted columns
- In order to download files the user must accept popups on the browser

**Steps to run the project** :

1. Clone the project : git clone https://gitlab.com/houssemmh/mamuntask.git
2. We need to build the backend for that we go under developer-evaluation folder and we run this command : mvn clean install
3. We create a volume : docker volume create --name=mamun-volume-task
4. We run the docker-compose : sudo docker-compose up -d --build
5. In order to test the application go to http://localhost:4200 
 
